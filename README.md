# Plantilla TI-I

Una plantilla hecha en LaTeX para el proyecto de investigación de la clase  Taller de Investigación I.

# Estructura de reporte final de residencia profesional

## Estructura general
- Portada
- Agradecimientos
- Resumen (Abstract)
- Índice
- Introducción

## Capítulo 1
- Generalidades del proyecto
  - Descripción de la empresa u organización y del puesto o área del trabajo de la que dependerá el estudiante (ubicación, fotos en primer plano)
- Problema a resolver enumerándolo en forma prioritaria
- Objetivo general y específico
- Justificación (por qué se va a realizar el proyecto, dinero)

## Capítulo 2
- Marco teórico

## Capítulo 3
- Desarrollo (procedimientos y procesos de las actividades realizadas)

## Capítulo 4
- Resultados (planos, gráficos, prototipo, manuales, programas, análisis estadísticos, modelos matemáticos, simulaciones, normatividades, regulaciones, restricciones, entre otros, estudio de mercado, estudio técnico, entre otros)

## Capítulo 5
- Conclusiones del proyecto
- Recomendaciones
- Experiencia profesional adquirida

## Capítulo 6
- Competencia desarrollada y aplicada

## Fuentes de información

## Anexos
- Carta de autorización por parte de la empresa
- Organización
- Registro de productos, patentes, erechos de autor, etc
